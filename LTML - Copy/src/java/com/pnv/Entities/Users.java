/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.Entities;

import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;



/**
 *
 * @author Administrator
 */
public class Users {
    @NotEmpty (message = "Please enter nick name")
    private String name;
    @NotEmpty(message = "Please enter password")
    @Size(min = 3, max = 15, message = "Your password must between 3 and 15 characters")
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
