<%-- 
    Document   : index
    Created on : May 6, 2015, 2:22:52 PM
    Author     : toitl
--%>
<%@ include file="./include.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link href="<c:url value="/resources/styles/admin.css" />" rel="stylesheet" type="text/css" />
          <link href="<c:url value="/resources/styles/general.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    <body>
        
        <div class="mainContainer">
            <div class="categories-menu-area">
                <a href="">Home</a>
                <a href="<%=request.getContextPath()%>/admin">Products</a>
                <a href="">Customer</a>
            </div>
            <div class="office-menu-area">
                <div class="home-tool">
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/homemn.png" />" /><br/>
                        Home
                    </div>
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/productmn.png" />" /><br/>
                        Product
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="main-content">
                
               <table>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Category</th>
        <th>Price</th>
        <th>Quantity</th>
        
        <th>Description</th>
        
        

        <c:forEach items="${listpro}" var="pro">  
        <tr>  
            <td><c:out value="${pro.proId}"/></td>  
            <td><c:out value="${pro.proName}"/></td>
            <td><c:out value="${pro.categories.catName}"/></td>
            <td><c:out value="${pro.proPrice}"/></td>  
            <td><c:out value="${pro.proQuantity}"/></td>
            <td><c:out value="${pro.proDescription}"/></td>
            
           
            <td align="center"><a href="<%=request.getContextPath()%>/department/edit?id=${pro.proId}">Edit</a> | <a href="<%=request.getContextPath()%>/department/delete?id=${pro.proId}">Delete</a></td>  
        </tr>  
    </c:forEach> 
</tr>

</table>
                
            </div>
        </div>
    </body>
</html>
