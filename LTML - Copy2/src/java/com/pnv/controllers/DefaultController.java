/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.Entities.Users;
import com.pnv.Validation.UserValidation;
import com.pnv.dao.ProductDao;
import com.pnv.models.Products;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author linhtn
 */
@Controller
public class DefaultController {
    
    List<Users> user = new ArrayList<Users>();
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(ModelMap map) {
        Users le = new Users();
        le.setName("My Le");
        le.setPassword("abc123");
        user.add(le);
        map.put("us", user);
        return "index";
    }
    
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap mm) {
        mm.addAttribute("us", new Users());
        return "login";
    }
   
    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    public String loginprocess(@Valid
            @ModelAttribute(value = "us") Users user1,
            BindingResult result, ModelMap mm) {
        UserValidation userValidation = new UserValidation();
        userValidation.validate(user1, result);

        if (result.hasErrors()) {
            mm.addAttribute("us", user1);
            return "login";
        }
        user.add(user1);
        mm.put("us", user);
        return "admin";
    }
    /**
     *
     * @param user1
     * @param result
     * @param mm
     * @return
     */
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String loginprocess2(@Valid
            @ModelAttribute(value = "us") Users user1,
            BindingResult result, ModelMap mm) {
        return "admin";
    }
    
}
