/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.CategoryDao;
import com.pnv.dao.ProductDao;
import com.pnv.models.Categories;
import com.pnv.models.Products;
import java.util.List;
import javax.swing.JOptionPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author linhtn
 */
@Controller
@RequestMapping(value = "products")
public class ProductController {
    @Autowired
    ProductDao productdao;
    
    public ProductController(){}
    
    @RequestMapping(method = RequestMethod.GET)
    public String runProduct(ModelMap map) {
        
        List<Products> listpro = productdao.findAll();
        map.put("listpro", listpro);
        return "products";
    }
    
    CategoryDao categorydao;
    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public String runCategory(ModelMap map) {
        List<Categories> listcate = categorydao.findAll();
        map.put("listCate", listcate);
        return "categories";
    }
}
