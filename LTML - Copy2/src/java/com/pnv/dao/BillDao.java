/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Bills;
import com.pnv.models.Categories;
import java.util.List;

/**
 *
 * @author linhtn
 */
public interface BillDao {
    public void saveOrUpdate(Bills bill);

    public void delete(Bills bill);

    public List<Bills> findAll();

    public Bills findByDepartmentId(int id);

    public Categories findByDepartmentCode(String categoryCode);
}
