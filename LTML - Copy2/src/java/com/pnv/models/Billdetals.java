package com.pnv.models;
// Generated May 13, 2015 8:43:58 AM by Hibernate Tools 3.2.1.GA


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Billdetals generated by hbm2java
 */
@Entity
@Table(name="billdetals"
    ,catalog="shoes_shop"
)
public class Billdetals  implements java.io.Serializable {


     private BilldetalsId id;
     private Products products;
     private Bills bills;
     private Integer bilQuantity;

    public Billdetals() {
    }

	
    public Billdetals(BilldetalsId id, Products products, Bills bills) {
        this.id = id;
        this.products = products;
        this.bills = bills;
    }
    public Billdetals(BilldetalsId id, Products products, Bills bills, Integer bilQuantity) {
       this.id = id;
       this.products = products;
       this.bills = bills;
       this.bilQuantity = bilQuantity;
    }
   
     @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="bilId", column=@Column(name="Bil_Id", nullable=false) ), 
        @AttributeOverride(name="proId", column=@Column(name="Pro_Id", nullable=false) ) } )
    public BilldetalsId getId() {
        return this.id;
    }
    
    public void setId(BilldetalsId id) {
        this.id = id;
    }
@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="Pro_Id", nullable=false, insertable=false, updatable=false)
    public Products getProducts() {
        return this.products;
    }
    
    public void setProducts(Products products) {
        this.products = products;
    }
@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="Bil_Id", nullable=false, insertable=false, updatable=false)
    public Bills getBills() {
        return this.bills;
    }
    
    public void setBills(Bills bills) {
        this.bills = bills;
    }
    
    @Column(name="Bil_Quantity")
    public Integer getBilQuantity() {
        return this.bilQuantity;
    }
    
    public void setBilQuantity(Integer bilQuantity) {
        this.bilQuantity = bilQuantity;
    }




}


