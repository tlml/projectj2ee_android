<%-- 
    Document   : index
    Created on : May 6, 2015, 2:22:52 PM
    Author     : toitl
--%>
<%@ include file="./include.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/styles/admin.css" />" rel="stylesheet" type="text/css" />
        <link href="<c:url value="/resources/styles/general.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    <body>

        <div class="mainContainer">
            <div class="categories-menu-area">
                <a href="<%=request.getContextPath()%>/admin">Home</a>
                <a href="<%=request.getContextPath()%>/products">Products</a>
                <a href="<%=request.getContextPath()%>/products">Categories</a>
            </div>
            <div class="office-menu-area">
                <div class="home-tool">
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/admin">
                            <img src="<c:url value="/resources/images/homemn.png" />" /><br/>
                            Home
                        </a>
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/products">
                            <img src="<c:url value="/resources/images/productmn.png" />" /><br/>
                            Product
                        </a>
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/categories">
                            <img src="<c:url value="/resources/images/catepmg.png" />" /><br/>
                            Categories
                        </a>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="main-content">

                <table border="1">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Description</th>



                        <c:forEach items="${listpro}" var="pro">  
                        <tr>  
                            <td><c:out value="${pro.proId}"/></td>  
                            <td><c:out value="${pro.proName}"/></td>
                            <td><c:out value="${pro.categories.catName}"/></td>
                            <td><c:out value="${pro.proPrice}"/></td>  
                            <td><c:out value="${pro.proQuantity}"/></td>
                            <td><c:out value="${pro.proDescription}"/></td>


                            <td align="center"><a href="<%=request.getContextPath()%>/department/edit?id=${pro.proId}">Edit</a> ||
                                <a href="<%=request.getContextPath()%>/department/delete?id=${pro.proId}">Delete</a></td>  
                        </tr>  
                    </c:forEach> 
                    </tr>

                </table>

            </div>
        </div>
    </body>
</html>
