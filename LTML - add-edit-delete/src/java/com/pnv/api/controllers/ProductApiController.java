/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controllers;

import com.pnv.dao.ProductDao;
import com.pnv.models.Products;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author linhtn
 */
@Controller
@RequestMapping(value = "api/products")
public class ProductApiController {
    @Autowired
    private ProductDao productdao;

    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Products> viewDepartmentPage() {
        /**
         * Get all titles
         */
        List<Products> listpro = productdao.findAll();
        return listpro;
    }
     @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public  @ResponseBody Products getByProductId(@PathVariable(value = "id") Integer id) {
        
//        Employees emp = new Employees();
//        emp.setEmpName("ddd");
//        emp.setAddress("ssss");
//        List<Employees> emp_lst = new ArrayList<Employees>();
        Products prod = productdao.findByProductId(id);
       // dep.setEmployees(emp_lst);
        return prod;
    }
}
