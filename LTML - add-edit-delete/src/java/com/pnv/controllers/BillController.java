/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.BillDao;
import com.pnv.models.Bills;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Administrator
 */

@Controller
@RequestMapping(value = "bills")
public class BillController {
    @Autowired
    BillDao billdao;
    
    @RequestMapping(value = "/bills", method = RequestMethod.GET)
    public String runCategory(ModelMap map) {
        List<Bills> listbill = billdao.findAll();
        map.put("listBill", listbill);
        return "bills";
    }
}
