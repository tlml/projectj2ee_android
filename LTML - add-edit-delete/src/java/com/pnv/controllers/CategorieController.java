/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.CategoryDao;
import com.pnv.models.Categories;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Administrator
 */

@Controller
@RequestMapping(value = "categories")
public class CategorieController {
    
    @Autowired
    CategoryDao categorydao;
    
    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public String runCategory(ModelMap map) {
        List<Categories> listcate = categorydao.findAll();
        map.put("listCate", listcate);
        return "categories";
    }
    
}
