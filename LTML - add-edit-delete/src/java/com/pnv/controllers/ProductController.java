/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.CategoryDao;
import com.pnv.dao.ProductDao;
import com.pnv.models.Products;
import com.pnv.utils.Constant;
import java.util.List;
import javax.swing.JOptionPane;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author linhtn
 */
@Controller
@RequestMapping(value = "products")
public class ProductController {
    @Autowired
    ProductDao productdao;
    
    
    public ProductController(){}
    
    @RequestMapping(method = RequestMethod.GET)
    public String runProduct(ModelMap map) {
        List<Products> listpro = productdao.findAll();
        map.put("listpro", listpro);
        return "products";
    }
      @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "proId", required = true) int proId, ModelMap map) {

        Products productForm = productdao.findByProductId(proId);
        map.addAttribute("productForm", productForm);
        return "product_add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Products productForm = new Products();
        map.addAttribute("productForm", productForm);
        return "product_add";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("productForm") Products productForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("productForm", productForm);
            return "product_add";
        }
        productdao.saveOrUpdate(productForm);

        /**
         * Get all titles
         */
        List<Products> listpro = productdao.findAll();
        map.put("listpro", listpro);
        map.addAttribute("add_success", "ok");

        return "products";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "proId", required = true) int proId, ModelMap map) {

        productdao.delete(productdao.findByProductId(proId));

        return Constant.REDIRECT + "/products";

    }
}
