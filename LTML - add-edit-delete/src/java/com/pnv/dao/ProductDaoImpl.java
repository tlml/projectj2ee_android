/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Products;
import com.pnv.utils.NewHibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author linhtn
 */
@Service
public class ProductDaoImpl implements ProductDao {
    //private final SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();

    @Override
    public void saveOrUpdate(Products product) {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(product);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Products product) {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(product);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Products> findAll() {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        List<Products> listpro = session.createQuery("from Products").list();
        session.close();
        return listpro;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public Products findByProductId(int proId) {
        Products product = null;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            product = (Products) session.get(Products.class, proId);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return product;
    }

    @Override
    public Products findByProductCode(String productCode) {
        Products product = null;
        String strQuery = "from Products WHERE Pro_Name = :depCode ";
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("depCode", productCode);
        product = (Products) query.uniqueResult();
        session.close();
        return product;
    }
}
