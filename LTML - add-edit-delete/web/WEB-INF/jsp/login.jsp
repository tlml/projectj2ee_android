<%-- 
    Document   : login
    Created on : May 11, 2015, 2:31:24 PM
    Author     : toitl
--%>
<%@ include file="./include.jsp" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link href="<c:url value="/resources/styles/login.css" />" rel="stylesheet" type="text/css" />
          <link href="<c:url value="/resources/styles/general.css" />" rel="stylesheet" type="text/css" />
           <link href="<c:url value="/resources/styles/styles.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body>
        <div class="loginContainer">
            <div class="advertising"></div>
            <div class="loginForm">
                <form:form action="admin" method="post" commandName="us">
                    
                    <form:errors path="name" cssClass="error"/><br/>
                    UserName: <form:input path="name" placeholder="Please input your nick"/>
                    <form:errors path="password" cssClass="error"/><br/>
                    Password: <form:input path="password" placeholder="Please input your password"/>
                    
                    <button type="submit">Login</button>
                </form:form>
            </div>
        </div>
    </body>
</html>
