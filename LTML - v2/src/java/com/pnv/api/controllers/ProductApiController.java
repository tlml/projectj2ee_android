/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controllers;

import com.pnv.dao.ProductDao;
import com.pnv.models.Products;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Administrator
 */
  @Controller
@RequestMapping(value = "api/Products")
public class ProductApiController {
   @Autowired
    private ProductDao productdao;
    public ProductApiController(){
        
    }
     @RequestMapping(method = RequestMethod.GET )
    public @ResponseBody    List<Products> getProductses() {
        /**
         * Get all titles
         */ 
        List<Products> titles_list = productdao.findAll();
        return titles_list;
    }
       @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public @ResponseBody    Products getProductByID( @PathVariable(value = "id") Integer id) {        
        return productdao.findByDepartmentId(id);
    }
       @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Products productForm = new Products();
        map.addAttribute("ProductForm", productForm);
        return "product-add";

    } 
}
