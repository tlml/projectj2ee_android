/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.BillDao;
import com.pnv.models.Billdetals;
import com.pnv.models.Bills;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping(value = "/bills")
public class BillController {
    @Autowired
    BillDao billdao;
    
    public BillController(){}
    
    @RequestMapping(method = RequestMethod.GET)
    public String runBill(ModelMap map) {
        
        List<Bills> listpro = billdao.findAll();
        map.put("listbill", listpro);
        return "bills";
    }
     @RequestMapping(value = "/billdetals", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "id", required = true) int id, ModelMap map) {
       List<Billdetals> listbilldetal = billdao.findBilldetail(id);
        map.put("listbilldetal", listbilldetal); 
       
      
        return "index";
    }
}
