/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.BillDao;
import com.pnv.dao.BilldetalDao;
import com.pnv.models.Billdetals;
import com.pnv.models.Bills;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping(value = "/billdetail")
public class BilldetailController {
    @Autowired
    BilldetalDao billdao;
    
    public BilldetailController(){}
    
    @RequestMapping(method = RequestMethod.GET)
    public String runBilldetail(ModelMap map) {
        
        List<Billdetals> listpro = billdao.findAll();
        map.put("listbill", listpro);
        return "bills";
    }
}
