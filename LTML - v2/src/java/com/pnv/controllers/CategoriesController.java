/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.BillDao;
import com.pnv.dao.CategoryDao;
import com.pnv.models.Bills;
import com.pnv.models.Categories;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping(value = "/categories")
public class CategoriesController {
       @Autowired
    CategoryDao categorydao;
    
    public CategoriesController(){}
    
    @RequestMapping(method = RequestMethod.GET)
    public String runCategories(ModelMap map) {
        
        List<Categories> listpro = categorydao.findAll();
        map.put("listCate", listpro);
        return "categories";
    }
    
}
