<%-- 
    Document   : index
    Created on : May 6, 2015, 2:22:52 PM
    Author     : toitl
--%>
<%@ include file="./include.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/styles/admin.css" />" rel="stylesheet" type="text/css" />
        <link href="<c:url value="/resources/styles/general.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    <body>
        <div class="mainContainer">
            <div class="categories-menu-area">
                <a href="<%=request.getContextPath()%>/home">Home</a>
                <a href="<%=request.getContextPath()%>/products">Products</a>
                <a href="<%=request.getContextPath()%>/categories">Categories</a>
                <a href="<%=request.getContextPath()%>/bills">Bills</a>
                <a href="<%=request.getContextPath()%>/billdetail">BillDetails</a>
            </div>
            <div class="office-menu-area">
                <div class="home-tool">
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/home"><img src="<c:url value="/resources/images/homemn.png" />" /></a><br/>
                        Home
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/products"> <img src="<c:url value="/resources/images/productmn.png" />" /><br/></a>
                        Product
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/categories"><img src="<c:url value="/resources/images/category.png" />" /><br/></a>
                        Categories
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/bills"> <img src="<c:url value="/resources/images/bill.jpg" />" /><br/></a>
                        Bills
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/billdetails"> <img src="<c:url value="/resources/images/detail.jpg" />" /><br/></a>
                        BillDetails
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="office-menu-area">
                <div class="home-tool">
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/add.jpg" />" /><br/>
                        Add
                    </div>
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/update.jpg" />" /><br/>
                        Update
                    </div>

                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/delete.jpg" />" /><br/>
                        Delete
                    </div>
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/expoet.png" />" /><br/>
                        Export
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="main-content">

            </div>
        </div>
    </body>
</html>
