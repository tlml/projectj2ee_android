/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.CategoryDao;
import com.pnv.models.Categories;
import com.pnv.models.Products;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author linhtn
 */
@Controller

@RequestMapping(value = "/categories")
public class CategoryController {
    @Autowired
    CategoryDao categorydao;
    @RequestMapping(method = RequestMethod.GET)
    public String runCategory(ModelMap map) {
        List<Categories> listcate = categorydao.findAll();
        map.put("listcate", listcate);
        return "categories";
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "id", required = true) int id, ModelMap map) {
         
       
        Categories categoryForm = categorydao.findByDepartmentId(id);
        map.addAttribute("categoryForm", categoryForm);
        return "category-add";
    
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {
       
       
      
        Categories categoryForm = new Categories();
        map.addAttribute("categoryForm",categoryForm);
        return "category-add";

    }
    @RequestMapping(value = "/kaito", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("categoryForm") Categories categoryForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("departmentForm", categoryForm);
            return "category-add";
        }
        categorydao.saveOrUpdate(categoryForm);

        /**
         * Get all titles
         */
        List<Categories> listcate = categorydao.findAll();
        map.put("listcate", listcate);
        map.addAttribute("add_success", "ok");

        return "categories";
    }
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "id", required = true) int id, ModelMap map) {

        categorydao.delete(categorydao.findByDepartmentId(id));
        List<Categories> listcate = categorydao.findAll();
        map.put("listcate", listcate);
        return  "categories";

    }
}
