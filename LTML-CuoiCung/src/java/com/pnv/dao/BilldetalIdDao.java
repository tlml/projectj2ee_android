/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.BilldetalsId;
import com.pnv.models.Bills;
import com.pnv.models.Categories;
import java.util.List;

/**
 *
 * @author linhtn
 */
public interface BilldetalIdDao {
    public void saveOrUpdate(BilldetalsId bill);

    public void delete(BilldetalsId bill);

    public List<BilldetalsId> findAll();

    public BilldetalsId findByDepartmentId(int id);

    public Categories findByDepartmentCode(String categoryCode);
}
