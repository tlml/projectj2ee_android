/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.ProductDao;
import com.pnv.models.Products;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author linhtn
 */
@Controller
public class DefaultController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String runFirst(ModelMap map) {
        return "product-add";
    }
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String displayAdmin(ModelMap map) {
        return "admin";
    }
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String displayLogin(ModelMap map) {
        return "login";
    }
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String impleLogin(ModelMap map) {
        return "admin";
    }
    
}
