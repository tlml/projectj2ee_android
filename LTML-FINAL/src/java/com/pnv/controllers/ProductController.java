/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.CategoryDao;
import com.pnv.dao.ProductDao;
import com.pnv.models.Categories;
import com.pnv.models.Products;
import java.util.List;
import javax.swing.JOptionPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author linhtn
 */
@Controller
@RequestMapping(value = "products")
public class ProductController {
    @Autowired
    ProductDao productdao;
    
    public ProductController(){}
    
    @RequestMapping(method = RequestMethod.GET)
    public String runProduct(ModelMap map) {
        
        List<Products> listpro = productdao.findAll();
        map.put("listpro", listpro);
        
        return "products";
    }
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteProduct(@RequestParam(value = "id", required = true) int id, ModelMap map) {

        productdao.delete(productdao.findByDepartmentId(id));
        List<Products> listpro = productdao.findAll();
        map.put("listpro", listpro);
        return "products";
    }
    /*@RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addProduct(ModelMap map) {

        Products productForm = new Products();
        map.addAttribute("productForm", productForm);
        return "product-add";

    }*/
    @RequestMapping(value = "/kaito", method = RequestMethod.POST)
    public String addProduct_(ModelMap map) {

        Products productForm = new Products();
        map.addAttribute("productForm", productForm);
        return "products";

    }
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "id", required = true) int id, ModelMap map) {

        Products departmentForm = productdao.findByDepartmentId(id);
        map.addAttribute("departmentForm", departmentForm);
        return "department-add";
    }
    /*@RequestMapping(value = "/categories", method = RequestMethod.GET)
    public String runCategory(ModelMap map) {
        
        CategoryDao categorydao = null;
        List<Categories> listpro = categorydao.findAll();
        map.put("listCate", listpro);
        
        return "categories";
    }*/
}
