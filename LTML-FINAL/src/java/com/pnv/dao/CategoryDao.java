/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Categories;
import com.pnv.models.Products;
import java.util.List;

/**
 *
 * @author linhtn
 */
public interface CategoryDao {
    public void saveOrUpdate(Categories category);

    public void delete(Categories category);

    public List<Categories> findAll();

    public Categories findByDepartmentId(int id);

    public Categories findByDepartmentCode(String categoryCode);
}
