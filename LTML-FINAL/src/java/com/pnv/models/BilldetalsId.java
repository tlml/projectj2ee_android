package com.pnv.models;
// Generated May 13, 2015 8:43:58 AM by Hibernate Tools 3.2.1.GA


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * BilldetalsId generated by hbm2java
 */
@Embeddable
public class BilldetalsId  implements java.io.Serializable {


     private int bilId;
     private int proId;

    public BilldetalsId() {
    }

    public BilldetalsId(int bilId, int proId) {
       this.bilId = bilId;
       this.proId = proId;
    }
   

    @Column(name="Bil_Id", nullable=false)
    public int getBilId() {
        return this.bilId;
    }
    
    public void setBilId(int bilId) {
        this.bilId = bilId;
    }

    @Column(name="Pro_Id", nullable=false)
    public int getProId() {
        return this.proId;
    }
    
    public void setProId(int proId) {
        this.proId = proId;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof BilldetalsId) ) return false;
		 BilldetalsId castOther = ( BilldetalsId ) other; 
         
		 return (this.getBilId()==castOther.getBilId())
 && (this.getProId()==castOther.getProId());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + this.getBilId();
         result = 37 * result + this.getProId();
         return result;
   }   


}


