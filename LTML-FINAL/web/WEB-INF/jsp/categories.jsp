<%-- 
    Document   : index
    Created on : May 6, 2015, 2:22:52 PM
    Author     : toitl
--%>
<%@ include file="./include.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link href="<c:url value="/resources/styles/admin.css" />" rel="stylesheet" type="text/css" />
          <link href="<c:url value="/resources/styles/general.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    <body>
        <div class="mainContainer">
            <div class="categories-menu-area">
                <a href="">Home</a>
                <a href="<%=request.getContextPath()%>/products">Products</a>
                <a href="">Customer</a>
            </div>
            <div class="office-menu-area">
                <div class="home-tool">
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/homemn.png" />" /><br/>
                        Home
                    </div>
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/productmn.png" />" /><br/>
                        Product
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="main-content">
                
                             <table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        
        
        <c:forEach items="${listCate}" var="pro">  
        <tr>  
            <td><c:out value="${pro.catId}"/></td>  
            <td><c:out value="${pro.catName}"/></td>
            
           
            <td align="center"><a href="<%=request.getContextPath()%>/department/edit?id=${pro.catId}">Edit</a> | <a href="<%=request.getContextPath()%>/department/delete?id=${pro.catId}">Delete</a></td>  
        </tr>  
    </c:forEach> 
</tr>

</table>
                
            </div>
        </div>
    </body>
</html>
