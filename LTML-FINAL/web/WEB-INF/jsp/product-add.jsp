<%-- 
    Document   : index
    Created on : May 6, 2015, 2:22:52 PM
    Author     : toitl
--%>
<%@ include file="./include.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link href="<c:url value="/resources/styles/admin.css" />" rel="stylesheet" type="text/css" />
          <link href="<c:url value="/resources/styles/general.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    <body>
        <div class="mainContainer">
            <div class="categories-menu-area">
                <a href="<%=request.getContextPath()%>">Home</a>
                <a href="<%=request.getContextPath()%>/products">Products</a>
                <a href="<%=request.getContextPath()%>/bills">Bills</a>
            </div>
            <div class="office-menu-area">
                <div class="home-tool">
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/homemn.png" />" /><br/>
                        Home
                    </div>
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/productmn.png" />" /><br/>
                        Product
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="main-content">
                <h1>Add New Product</h1>

<div align="center">
    
    <table border="0" width="90%">
        <form:form action="kaito" modelAttribute="productForm" method="POST" >
            <form:hidden path="proId" />
            <tr>
                <td align="left" width="20%">Id: </td>
                <td align="left" width="40%"><form:input path="proId" size="30"/></td>
                <td align="left"><form:errors path="proId" cssClass="error"/></td>
            </tr>
            <tr>
                <td align="left" width="20%">Name: </td>
                <td align="left" width="40%"><form:input path="proName" size="30"/></td>
                <td align="left"><form:errors path="proName" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Price: </td>
                <td><form:input path="proPrice" size="30"/></td>
                <td><form:errors path="proPrice" cssClass="error"/></td>
            </tr>
           <tr>
                <td>Quantiy: </td>
                <td><form:input path="proQuantity" size="30"/></td>
                <td><form:errors path="proQuantity" cssClass="error"/></td>
            </tr>
            
            <tr>
                <td>Description: </td>
                <td><form:input path="proDescription" size="30"/></td>
                <td><form:errors path="proDescription" cssClass="error"/></td>
            </tr>

                    <tr>
                        <td><a href="<%=request.getContextPath()%>/products" class="button">Back</a></td>
                        <td align="center"><input type="submit" value="submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
            </div>
            </div>
        </div>
    </body>
</html>
