/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controllers;

import com.pnv.dao.CategoryDao;
import com.pnv.dao.ProductDao;
import com.pnv.models.Categories;
import com.pnv.models.Products;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author linhtn
 */
@Controller
@RequestMapping(value = "api/categories")
public class CatogoryApiController {
    @Autowired
    private CategoryDao categorydao;

    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Categories> viewDepartmentPage() {
        /**
         * Get all titles
         */
        List<Categories> departments_list = categorydao.findAll();
        return departments_list;
    }
     @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public  @ResponseBody Categories getDepartmentByID(@PathVariable(value = "id") Integer id) {
        
//        Employees emp = new Employees();
//        emp.setEmpName("ddd");
//        emp.setAddress("ssss");
//        List<Employees> emp_lst = new ArrayList<Employees>();
        Categories prod = categorydao.findByDepartmentId(id);
       // dep.setEmployees(emp_lst);
        return prod;
    }
}
