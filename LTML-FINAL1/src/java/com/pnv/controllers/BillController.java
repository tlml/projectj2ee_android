/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.BillDao;
import com.pnv.dao.ProductDao;
import com.pnv.models.Billdetals;
import com.pnv.models.Bills;
import com.pnv.models.Products;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author linhtn
 */
@Controller
@RequestMapping(value = "/bills")
public class BillController {
    @Autowired
    BillDao billdao;
    
    public BillController(){}
    
    @RequestMapping(method = RequestMethod.GET)
    public String runBill(ModelMap map) {
        
        List<Bills> listpro = billdao.findAll();
        map.put("listbill", listpro);
        
        return "bills";
    }
    @RequestMapping(value = "/billdetals", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "id", required = true) int id, ModelMap map) {
       Bills bill = billdao.findByDepartmentId(id);
       List<Billdetals> listbill = billdao.findListBilldetail(bill);
        map.put("listbilldetal", listbill); 
       
      
        return "billdetals";
    }
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteProduct(@RequestParam(value = "id", required = true) int id, ModelMap map) {

        billdao.delete(billdao.findByDepartmentId(id));
        List<Bills> listpro = billdao.findAll();
        map.put("listpro", listpro);
        return "products";
    }
}
