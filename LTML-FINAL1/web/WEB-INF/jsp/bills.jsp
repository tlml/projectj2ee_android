<%-- 
    Document   : index
    Created on : May 6, 2015, 2:22:52 PM
    Author     : toitl
--%>
<%@ include file="./include.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link href="<c:url value="/resources/styles/admin.css" />" rel="stylesheet" type="text/css" />
          <link href="<c:url value="/resources/styles/general.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    <body>
        
        <div class="mainContainer">
            <div class="categories-menu-area">
                <a href="">Home</a>
                <a href="<%=request.getContextPath()%>/admin">Products</a>
                <a href="<%=request.getContextPath()%>/bills">Bills</a>
            </div>
            <div class="office-menu-area">
                <div class="home-tool">
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/homemn.png" />" /><br/>
                        Home
                    </div>
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/productmn.png" />" /><br/>
                        Product
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="main-content">
                
               <table>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Total Price</th>
        <th>Date</th>
        <th>Note</th>
        
       
        
        

        <c:forEach items="${listbill}" var="pro">  
        <tr>  
            <td><c:out value="${pro.bilId}"/></td>  
            <td><c:out value="${pro.bilName}"/></td>
            <td><c:out value="${pro.bilTotalPrice}"/></td>
            <td><c:out value="${pro.bilDate}"/></td>  
            <td><c:out value="${pro.bilNote}"/></td>
           
            
           
            <td align="center"><a href="<%=request.getContextPath()%>/bills/billdetals?id=${pro.bilId}">Detail</a><a href="<%=request.getContextPath()%>/bills/edit?id=${pro.bilId}">Edit</a> | <a href="<%=request.getContextPath()%>/bills/delete?id=${pro.bilId}">Delete</a></td>  
        </tr>  
    </c:forEach> 
</tr>

</table>
                
            </div>
        </div>
    </body>
</html>
