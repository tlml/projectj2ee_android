<%-- 
    Document   : login
    Created on : May 11, 2015, 2:31:24 PM
    Author     : toitl
--%>
<%@ include file="./include.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link href="<c:url value="/resources/styles/login.css" />" rel="stylesheet" type="text/css" />
          <link href="<c:url value="/resources/styles/general.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    </head>
    <body>
        <div class="loginContainer">
            <div class="advertising"></div>
            <div class="loginForm">
                <form action="login" method="post">
                    <input class="user" name="nick" type="text" placeholder="Please input your nick"/>
                    <input class="password" name="password" type="password" placeholder="Please input your password"/>
                    <button type="submit">Login</button>
                </form>
            </div>
        </div>
    </body>
</html>
