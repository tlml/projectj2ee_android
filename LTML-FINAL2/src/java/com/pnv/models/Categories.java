package com.pnv.models;
// Generated May 13, 2015 8:43:58 AM by Hibernate Tools 3.2.1.GA

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Categories generated by hbm2java
 */
@Entity
@Table(name = "categories", catalog = "shoes_shop")
public class Categories implements java.io.Serializable {

    private Integer catId;
    private String catName;
    private List<Products> productses;
    //private Set productses = new HashSet(0);

    public Categories() {
    }

    public Categories(String catName, List<Products> productses) {
        this.catName = catName;
        this.productses = productses;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Cat_Id", unique = true, nullable = false)
    public Integer getCatId() {
        return this.catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    @Column(name = "Cat_Name", length = 50)
    public String getCatName() {
        return this.catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "categories")
    public List<Products> getProductses() {
        return this.productses;
    }

    public void setProductses(List<Products> productses) {
        this.productses = productses;
    }
}
