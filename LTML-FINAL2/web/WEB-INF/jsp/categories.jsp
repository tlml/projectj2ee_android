<%-- 
    Document   : index
    Created on : May 6, 2015, 2:22:52 PM
    Author     : toitl
--%>
<%@ include file="./include.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link href="<c:url value="/resources/styles/admin.css" />" rel="stylesheet" type="text/css" />
          <link href="<c:url value="/resources/styles/general.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    <body>
        
        <div class="mainContainer">
            <div class="categories-menu-area">
                <a href="">Home</a>
                <a href="<%=request.getContextPath()%>/admin">Products</a>
                <a href="<%=request.getContextPath()%>/bills">Bills</a>
                <a href="<%=request.getContextPath()%>/categories">Categories</a>
            </div>
            <div class="office-menu-area">
                <div class="home-tool">
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/homemn.png" />" /><br/>
                        Home
                    </div>
                    <div class="box-tool">
                        <img src="<c:url value="/resources/images/productmn.png" />" /><br/>
                        Product
                    </div>
                        
                    <div class="clr"></div>
                </div>
                        
            </div>
                        
            <div class="main-content">
                
               <table>
    <tr>
        <th>ID</th>
        <th>Name</th>
       
        
       
        
        

        <c:forEach items="${listcate}" var="pro">  
        <tr>  
            <td><c:out value="${pro.catId}"/></td>  
            <td><c:out value="${pro.catName}"/></td>                       
            <td align="center"><a href="<%=request.getContextPath()%>/categories/edit?id=${pro.catId}">edit</a><a href="<%=request.getContextPath()%>/categories/delete?id=${pro.catId}">Delete</a></td>
        </tr>  
    </c:forEach> 
</tr>

</table>
                <div>
                        <a href="<%=request.getContextPath()%>/categories/add">Add new a Category</a>
                        </div>
                
            </div>
        </div>
    </body>
</html>
