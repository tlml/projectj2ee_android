<%-- 
    Document   : index
    Created on : May 6, 2015, 2:22:52 PM
    Author     : toitl
--%>
<%@ include file="./include.jsp" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/styles/admin.css" />" rel="stylesheet" type="text/css" />
        <link href="<c:url value="/resources/styles/general.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    <body>
        <div class="mainContainer">
            <div class="categories-menu-area">
                <a href="<%=request.getContextPath()%>/admin">Home</a>
                <a href="<%=request.getContextPath()%>/products">Products</a>
                <a href="<%=request.getContextPath()%>/categories">Categories</a>
                <a href="<%=request.getContextPath()%>/bills">Bills</a>
                <a href="<%=request.getContextPath()%>/billdetals">Billdetals</a>
            </div>
            <div class="office-menu-area">
                <div class="home-tool">
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/admin">
                            <img src="<c:url value="/resources/images/homemn.png" />" /><br/>
                            Home
                        </a>
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/products">
                            <img src="<c:url value="/resources/images/productmn.png" />" /><br/>
                            Product
                        </a>
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/categories">
                            <img src="<c:url value="/resources/images/catepmg.png" />" /><br/>
                            Categories
                        </a>
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/bills">
                            <img src="<c:url value="/resources/images/billpmg.png" />" /><br/>
                            Bills
                        </a>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="main-content">

                <table border="0" width="90%">
                    <form:form action="kaito" modelAttribute="billdetalForm" method="POST" >
                        <form:hidden path="bilDetalId" />
                        
                        <tr>
                            <td align="left" width="20%">Bill: </td>
                            <td align="left" width="40%">
                                <form:select path="bills.bilId">
            		<form:option value="" label="--Please Select"/>
            		<form:options items="${listbill}" itemValue="bilId" itemLabel="bilName"/>
        		</form:select>
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="20%">Product: </td>
                            <td align="left" width="40%">
                                <form:select path="products.proId">
            		<form:option value="" label="--Please Select"/>
            		<form:options items="${listpro}" itemValue="proId" itemLabel="proName"/>
        		</form:select>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>Quantity: </td>
                            <td><form:input path="bilQuantity" size="30"/></td>
                            <td><form:errors path="bilQuantity" cssClass="error"/></td>
                        </tr>
                        
                        
                       
                        
                      
                        
                        <tr>
                            
                            <td align="center"><input type="submit" value="submit"/></td>
                            <td></td>
                        </tr>
                    </form:form>
                </table>

            </div>
        </div>
    </body>
</html>
