<%-- 
    Document   : index
    Created on : May 6, 2015, 2:22:52 PM
    Author     : toitl
--%>
<%@ include file="./include.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/styles/admin.css" />" rel="stylesheet" type="text/css" />
        <link href="<c:url value="/resources/styles/general.css" />" rel="stylesheet" type="text/css" />
        <title>JSP Page</title>
    <body>

        <div class="mainContainer">
            <div class="categories-menu-area">
                <a href="<%=request.getContextPath()%>/admin">Home</a>
                <a href="<%=request.getContextPath()%>/products">Products</a>
                <a href="<%=request.getContextPath()%>/categories">Categories</a>
                <a href="<%=request.getContextPath()%>/bills">Bills</a>
                <a href="<%=request.getContextPath()%>/billdetals">Billdetals</a>
            </div>
            <div class="office-menu-area">
                <div class="home-tool">
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/admin">
                            <img src="<c:url value="/resources/images/homemn.png" />" /><br/>
                            Home
                        </a>
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/products">
                            <img src="<c:url value="/resources/images/productmn.png" />" /><br/>
                            Product
                        </a>
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/categories">
                            <img src="<c:url value="/resources/images/catepmg.png" />" /><br/>
                            Categories
                        </a>
                    </div>
                    <div class="box-tool">
                        <a href="<%=request.getContextPath()%>/bills">
                            <img src="<c:url value="/resources/images/billpmg.png" />" /><br/>
                            Bills
                        </a>
                    </div>
                    <div class="clr"></div>
                </div>

            </div>

            <div class="main-content">
                <h3>${add_success}</h3>
                <form:form action="searchcate" modelAttribute="catesearch" method="POST" >  
                        <tr>
                            <td align="center" width="20%">choose category: </td>
                            <td align="center" width="40%">
                                <form:select path="categories.catId">
            		<form:option value="" label="--Please Select"/>
            		<form:options items="${listcate2}" itemValue="catId" itemLabel="catName"/>
        		</form:select>
                                
                            </td>
                           <td align="center"><input type="submit" value="Search"/></td>
                        </tr>
                </form:form>
                <a href="<%=request.getContextPath()%>/categories/add" class="button">Add New</a>
                <table>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <c:forEach items="${listcate}" var="pro">  
                        <tr>  
                            <td><c:out value="${pro.catId}"/></td>  
                            <td><c:out value="${pro.catName}"/></td>                       
                            <td align="center"><a href="<%=request.getContextPath()%>/categories/edit?id=${pro.catId}">edit</a>||
                                <a href="<%=request.getContextPath()%>/categories/delete?id=${pro.catId}">Delete</a></td>
                        </tr>  
                    </c:forEach> 
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
