/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controllers;

import com.pnv.dao.BilldetalDao;
import com.pnv.models.Billdetals;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author linhtn
 */
@Controller
@RequestMapping(value = "/api/billdetals")
public class BilldetalApiController {
    @Autowired
    BilldetalDao billdetaldao;
    @RequestMapping(method = RequestMethod.GET)
    public  @ResponseBody List<Billdetals> ab() {
        List<Billdetals> listbilldetal = billdetaldao.findAll();
        return listbilldetal;
    }
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public  @ResponseBody List<Billdetals> jab(@PathVariable(value = "id") Integer id) {
        List<Billdetals> listbilldetal = billdetaldao.findone(id);
        return listbilldetal;
}
}
