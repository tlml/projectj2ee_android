/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.BillDao;
import com.pnv.dao.ProductDao;
import com.pnv.models.BilSearch;
import com.pnv.models.Billdetals;
import com.pnv.models.Bills;
import com.pnv.models.Categories;
import com.pnv.models.DetalSearch;
import com.pnv.models.Products;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author linhtn
 */
@Controller
@RequestMapping(value = "/bills")
public class BillController {
    @Autowired
    BillDao billdao;
    
    public BillController(){}
    
    @RequestMapping(method = RequestMethod.GET)
    public String runBill(ModelMap map) {
        BilSearch bilsearch = new BilSearch();
        List<Bills> listbil2 = billdao.findAll();
        map.addAttribute("listbil2", listbil2);
        map.addAttribute("bilsearch", bilsearch);
        List<Bills> listpro = billdao.findAll();
        map.put("listbill", listpro);
        
        return "bills";
    }
    @RequestMapping(value = "/billdetals", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "id", required = true) int id, ModelMap map) {
       Bills bill = billdao.findByDepartmentId(id);
       List<Billdetals> listbill = billdao.findListBilldetail(bill);
        map.put("listbilldetal", listbill); 
       List<Bills> listbill2 = billdao.findAll();
        map.addAttribute("listbill2", listbill2);
        DetalSearch detalsearch = new DetalSearch();
        map.addAttribute("detalsearch", detalsearch);
      
        return "billdetals";
    }
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String aviewEditTitlePage(@RequestParam(value = "id", required = true) int id, ModelMap map) {
         
       
        Bills billForm = billdao.findByDepartmentId(id);
        map.addAttribute("billForm", billForm);
        return "bill-add";
    
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {
       
       
      
        Bills billForm = new Bills();
        map.addAttribute("billForm",billForm);
        return "bill-add";

    }
    @RequestMapping(value = "/kaito", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("billForm") Bills billForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("departmentForm", billForm);
            return "bill-add";
        }
        billdao.saveOrUpdate(billForm);

        /**
         * Get all titles
         */
        List<Bills> listbill = billdao.findAll();
        map.put("listbill", listbill);
        map.addAttribute("add_success", "ok");
       BilSearch bilsearch = new BilSearch();
        List<Bills> listbil2 = billdao.findAll();
        map.addAttribute("listbil2", listbil2);
        map.addAttribute("bilsearch", bilsearch);
        return "bills";
    }
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "id", required = true) int id, ModelMap map) {
        BilSearch bilsearch = new BilSearch();
        List<Bills> listbil2 = billdao.findAll();
        map.addAttribute("listbil2", listbil2);
        map.addAttribute("bilsearch", bilsearch);
        billdao.delete(billdao.findByDepartmentId(id));
        List<Bills> listbill = billdao.findAll();
        map.put("listbill", listbill);
        return  "bills";

    }
}
