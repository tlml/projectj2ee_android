/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.BillDao;
import com.pnv.dao.BilldetalDao;
import com.pnv.dao.ProductDao;
import com.pnv.models.BilSearch;
import com.pnv.models.Billdetals;
import com.pnv.models.Bills;
import com.pnv.models.Categories;
import com.pnv.models.DetalSearch;
import com.pnv.models.Products;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author linhtn
 */
@Controller
@RequestMapping(value = "/billdetals")
public class BilldetalController {
    @Autowired
    BillDao billdao;
    
    @Autowired
    ProductDao productdao;
    @Autowired
    BilldetalDao billdetaldao;
    @RequestMapping(method = RequestMethod.GET)
    public String abc(ModelMap map) {
        List<Billdetals> listbilldetal = billdetaldao.findAll();
        List<Bills> listbill = billdao.findAll();
        map.addAttribute("listbill2", listbill);
        DetalSearch detalsearch = new DetalSearch();
        map.addAttribute("detalsearch", detalsearch);
        map.addAttribute("listbilldetal", listbilldetal);
        return "billdetals";
    }
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String run(ModelMap map) {
        Billdetals billdetalForm = new Billdetals();
        List<Bills> listbill = billdao.findAll();
        map.addAttribute("listbill", listbill);
        map.addAttribute("billdetalForm", billdetalForm);
        List<Products> listpro = productdao.findAll();
        map.addAttribute("listpro", listpro);
        return "billdetal-add";
    }
    @RequestMapping(value = "/kaito", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("billdetalForm") Billdetals billdetalForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("departmentForm", billdetalForm);
            return "bills";
        }
        billdetaldao.saveOrUpdate(billdetalForm);

        /**
         * Get all titles
         */
        
        List<Bills> listbill = billdao.findAll();
        map.put("listbill", listbill);
        map.addAttribute("add_success", "ok");
        BilSearch bilsearch = new BilSearch();
        List<Bills> listbil2 = billdao.findAll();
        map.addAttribute("listbil2", listbil2);
        map.addAttribute("bilsearch", bilsearch);
        return "bills";
    }
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "id", required = true) int id, ModelMap map) {

        billdetaldao.delete(billdetaldao.findByDepartmentId(id));
        List<Bills> listbilldetal = billdao.findAll();
        map.put("listbill", listbilldetal);
        BilSearch bilsearch = new BilSearch();
        List<Bills> listbil2 = billdao.findAll();
        map.addAttribute("listbil2", listbil2);
        map.addAttribute("bilsearch", bilsearch);
        return  "bills";

    }
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String aviewEditTitlePage(@RequestParam(value = "id", required = true) int id, ModelMap map) {
         
       
        Billdetals billdetalForm = billdetaldao.findByDepartmentId(id);
        map.addAttribute("billdetalForm", billdetalForm);
        return "billdetal-add";
    
    }
}
