/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.Entities.Users;
import com.pnv.Validation.UserValidation;
import com.pnv.dao.BillDao;
import com.pnv.dao.BilldetalDao;
import com.pnv.dao.CategoryDao;
import com.pnv.dao.ProductDao;
import com.pnv.models.BilSearch;
import com.pnv.models.Billdetals;
import com.pnv.models.Bills;
import com.pnv.models.CateSearch;
import com.pnv.models.Categories;
import com.pnv.models.DetalSearch;
import com.pnv.models.ProSearch;
import com.pnv.models.Products;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author linhtn
 */
@Controller
public class DefaultController {
//    @RequestMapping(value = "/", method = RequestMethod.GET)
//    public String runFirst(ModelMap map) {
//        return "login";
//    }
//    @RequestMapping(value = "/admin", method = RequestMethod.GET)
//    public String displayAdmin(ModelMap map) {
//        return "admin";
//    }
//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public String displayLogin(ModelMap map) {
//        return "login";
//    }
//    @RequestMapping(value = "/login", method = RequestMethod.POST)
//    public String impleLogin(ModelMap map) {
//        return "admin";
//    }
//    
    @Autowired
     BilldetalDao billdetaldao;
    @Autowired
     ProductDao productdao;
    @Autowired
     BillDao billdao;
    @Autowired
     CategoryDao categorydao;
     List<Users> user = new ArrayList<Users>();
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(ModelMap map) {
        Users le = new Users();
        le.setName("My Le");
        le.setPassword("abc123");
        user.add(le);
        map.put("us", user);
        return "index";
    }
    
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap mm) {
        mm.addAttribute("us", new Users());
        return "login";
    }
   
    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    public String loginprocess(@Valid
            @ModelAttribute(value = "us") Users user1,
            BindingResult result, ModelMap mm) {
        UserValidation userValidation = new UserValidation();
        userValidation.validate(user1, result);

        if (result.hasErrors()) {
            mm.addAttribute("us", user1);
            return "login";
        }
        user.add(user1);
        mm.put("us", user);
        return "admin";
    }
    /**
     *
     * @param user1
     * @param result
     * @param mm
     * @return
     */
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String loginprocess2(@Valid
            @ModelAttribute(value = "us") Users user1,
            BindingResult result, ModelMap mm) {
        return "admin";
    }
    
    @RequestMapping(value = "searchpro", method = RequestMethod.POST)
    public String search(@Valid @ModelAttribute("prosearch") ProSearch prosearch,
            BindingResult result, ModelMap map) {
        int id = prosearch.getProducts().getProId();
        
        
        List<Products> listpro = productdao.findone(id);
         map.addAttribute("listpro", listpro);
        List<Products> listpro2 = productdao.findAll();
        map.addAttribute("listpro2", listpro2);
        map.addAttribute("prosearch", prosearch);
        return "products";
    }
    @RequestMapping(value = "searchbil", method = RequestMethod.POST)
    public String searchbil(@Valid @ModelAttribute("bilsearch") BilSearch bilsearch,
            BindingResult result, ModelMap map) {
        int id = bilsearch.getBills().getBilId();
        
        String data = bilsearch.getAll();
        List<Bills> listbill = billdao.findone(id);
         map.addAttribute("listbill", listbill);
        List<Bills> listbil2 = billdao.findAll();
        map.addAttribute("listbil2", listbil2);
        map.addAttribute("bilsearch", bilsearch);
        return "bills";
    }
    @RequestMapping(value = "searchcate", method = RequestMethod.POST)
    public String searchbil(@Valid @ModelAttribute("catesearch") CateSearch catesearch,
            BindingResult result, ModelMap map) {
        int id = catesearch.getCategories().getCatId();
        
        String data = catesearch.getAll();
        List<Categories> listcate = categorydao.findone(id);
         map.addAttribute("listcate", listcate);
        List<Categories> listcate2 = categorydao.findAll();
        map.addAttribute("listcate2", listcate2);
        map.addAttribute("catesearch", catesearch);
        return "categories";
    }
    @RequestMapping(value = "searchdetal", method = RequestMethod.POST)
    public String searchbil(@Valid @ModelAttribute("detalsearch") DetalSearch detalsearch,
            BindingResult result, ModelMap map) {
        int id = detalsearch.getBills().getBilId();
        List<Billdetals> listbilldetal = billdetaldao.findone(id);
        map.addAttribute("listbilldetal", listbilldetal);
        List<Bills> listbill2 = billdao.findAll();
        map.addAttribute("listbill2", listbill2);
        map.addAttribute("detalsearch", detalsearch);
        return "billdetals";
    }
}
