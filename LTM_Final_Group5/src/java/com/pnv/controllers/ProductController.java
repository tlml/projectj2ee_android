/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.CategoryDao;
import com.pnv.dao.ProductDao;
import com.pnv.models.Categories;
import com.pnv.models.ProSearch;
import com.pnv.models.Products;
import com.pnv.utils.Constant;
import java.util.List;
import javax.swing.JOptionPane;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author linhtn
 */
@Controller
@RequestMapping(value = "products")
public class ProductController {
    @Autowired
    ProductDao productdao;
     @Autowired
        CategoryDao categorydao;
     public ProductController(){}
    
    @RequestMapping(method = RequestMethod.GET)
    public String runProduct(ModelMap map) {
        List<Products> listpro = productdao.findAll();
        map.put("listpro", listpro);
        ProSearch prosearch = new ProSearch();
        map.addAttribute("prosearch", prosearch);
        List<Products> listpro2 = productdao.findAll();
        map.addAttribute("listpro2", listpro2);
       
        return "products";
    }
      @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "id", required = true) int id, ModelMap map) {
          List<Categories> listcate = categorydao.findAll();
        map.addAttribute("listcate", listcate);
        Products productForm = productdao.findByProductId(id);
        map.addAttribute("productForm", productForm);
        return "product-add";
    
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {
       
        List<Categories> listcate = categorydao.findAll();
        map.addAttribute("listcate", listcate);
        Products productForm = new Products();
        map.addAttribute("productForm", productForm);
        return "product-add";

    }

    @RequestMapping(value = "/kaito", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("productForm") Products productForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("departmentForm", productForm);
            return "product-add";
        }
        productdao.saveOrUpdate(productForm);

        /**
         * Get all titles
         */
        List<Products> listpro = productdao.findAll();
        map.put("listpro", listpro);
        map.addAttribute("add_success", "ok");
        ProSearch prosearch = new ProSearch();
        map.addAttribute("prosearch", prosearch);
        List<Products> listpro2 = productdao.findAll();
        map.addAttribute("listpro2", listpro2);
        return "products";
    }
    

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "id", required = true) int id, ModelMap map) {

        productdao.delete(productdao.findByProductId(id));
        List<Products> listpro = productdao.findAll();
        map.put("listpro", listpro);
       ProSearch prosearch = new ProSearch();
        map.addAttribute("prosearch", prosearch);
        List<Products> listpro2 = productdao.findAll();
        map.addAttribute("listpro2", listpro2);
        return  "products";

    }
}
