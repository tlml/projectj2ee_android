/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Billdetals;
import com.pnv.models.Bills;
import com.pnv.models.Categories;
import java.util.List;

/**
 *
 * @author linhtn
 */
public interface BilldetalDao {
    public void saveOrUpdate(Billdetals bill);
    public List<Billdetals> findone(int id);
    public void delete(Billdetals bill);

    public List<Billdetals> findAll();

    public Billdetals findByDepartmentId(int id);

    public Billdetals findByDepartmentCode(String categoryCode);
}
