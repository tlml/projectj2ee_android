/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Billdetals;
import com.pnv.models.Bills;
import com.pnv.models.Products;
import com.pnv.utils.NewHibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author linhtn
 */
@Service
public class BilldetalDaoImpl implements BilldetalDao{
    //private final SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
    @Override
    public void saveOrUpdate(Billdetals bill) {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(bill);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
    @Override
    public List<Billdetals> findone(int id) {
       
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        List<Billdetals> departmentsList = session.createQuery("from Billdetals where Bil_Id = " + id).list();
        session.close();
        return departmentsList;
    }
    @Override
    public void delete(Billdetals bill) {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(bill);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Billdetals> findAll() {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        List<Billdetals> departmentsList = session.createQuery("from Billdetals").list();
        session.close();
        return departmentsList;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Billdetals findByDepartmentId(int id) {
        Billdetals bill = null;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            bill = (Billdetals) session.get(Billdetals.class, id);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return bill;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Billdetals findByDepartmentCode(String categoryCode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
