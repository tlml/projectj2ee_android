/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Categories;
import com.pnv.models.Products;
import com.pnv.utils.NewHibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author linhtn
 */
@Service
public class CategoryDaoImpl implements CategoryDao{
    //private final SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
    @Override
    public void saveOrUpdate(Categories category) {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(category);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public List<Categories> findone(int id) {
       
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        List<Categories> departmentsList = session.createQuery("from Categories where Cat_Id = " + id).list();
        session.close();
        return departmentsList;
    }
     @Override
     public List<Categories> search(String data) {
         Session session = NewHibernateUtil.getSessionFactory().openSession();
        List<Categories> listpro = session.createQuery("from Categories where Cat_Name LIKE '%" + data + "%'").list();
        session.close();
        return listpro;
     }
    @Override
    public void delete(Categories category) {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(category);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Categories> findAll() {
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        List<Categories> departmentsList = session.createQuery("from Categories").list();
        session.close();
        return departmentsList;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Categories findByDepartmentId(int id) {
       Categories category = null;
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            category = (Categories) session.get(Categories.class, id);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return category;
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Categories findByDepartmentCode(String categoryCode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
