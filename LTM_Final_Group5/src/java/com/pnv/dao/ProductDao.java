/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Products;
import java.util.List;

/**
 *
 * @author linhtn
 */
public interface ProductDao {
    public void saveOrUpdate(Products products);
    public List<Products> search(String data);
    public void delete(Products product);
     public List<Products> findone(int id);
    public List<Products> findAll();

    public Products findByProductId(int id);

    public Products findByProductCode(String productCode);
}
