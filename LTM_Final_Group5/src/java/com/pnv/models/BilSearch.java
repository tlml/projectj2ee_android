/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.models;

/**
 *
 * @author linhtn
 */
public class BilSearch {
    String all;
    Bills bills;

    public Bills getBills() {
        return bills;
    }

    public void setBills(Bills bills) {
        this.bills = bills;
    }
    public BilSearch() {
    }

    public BilSearch(String all) {
        this.all = all;
    }

    public String getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = all;
    }
    
}
